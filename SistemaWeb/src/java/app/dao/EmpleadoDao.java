/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.dao;

import app.entidad.Empleado;
import base.datos.BaseDatos;

/**
 *
 * @author ANDERSSON
 */
public class EmpleadoDao {

    
    private Empleado empleado;

    public EmpleadoDao() {
    }
    
    
    public EmpleadoDao(Empleado e) {
       
        this.empleado=e;
    }

    public void registrar() throws Exception {

        BaseDatos.conectar();
        String sql="INSERT INTO empleado (nombre,apellido,dni,direccion,telefono,estado) ";
        sql+="VALUES('"+empleado.getNombre()+"',";
        sql+=empleado.getApellido()+","+empleado.getDni()+","+empleado.getDireccion()+","+
             empleado.getTelefono()+","+empleado.getEstado()+")";
        BaseDatos.ejecutar(sql);
        BaseDatos.desconectar();
    }
    
}
